#!/usr/bin/env python3

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse
import os
import sys
from pathlib import Path

import yaml


"""
This scripts helps to indent the yaml files for each faction
equipments.yml (list of weapons and wargear)
models.yml
units.yml
rules.yml
psychics.yml
"""


class YamlModel(dict):
    def to_omap(self):
        order = [
            "speed",
            "cc",
            "bs",
            "defense",
            "hitpoints",
            "equipment",
            "special",
        ]
        default = {"hitpoint": 1}
        if "defense" in self:
            if self["defense"] > 10:
                raise ValueError("Model defense should be <= 10")
            elif self["defense"] < 2:
                raise ValueError("Model defense should be >= 2")
        return [
            (key, self[key])
            for key in order
            if key in self and (key not in default or self[key] != default[key])
        ]


class YamlUnit(dict):
    def to_omap(self):
        order = [
            "models",
            "min",
            "max",
        ]
        default = {"min": 0, "max": 1}
        return [
            (key, self[key])
            for key in order
            if key in self and (key not in default or self[key] != default[key])
        ]


class YamlSection(dict):
    def to_omap(self):
        return ((key, self[key]) for key in sorted(self))


class YamlUpgrade(dict):
    def to_omap(self):
        order = ["models", "add", "remove", "text", "all", "max", "max_model"]
        return [(key, self[key]) for key in order if key in self]


class YamlEquipments(dict):
    def to_omap(self):
        order = ["weapons", "wargear"]
        return [(key, self[key]) for key in order if key in self]


class YamlWeapon(dict):
    def to_omap(self):
        order = ["range", "attacks", "ap", "damage", "special"]
        default = {"range": 0, "ap": 0, "damage": 1, "special": []}
        if "ap" in self and self["ap"] > 7:
            raise ValueError("AP cannot be higher than 7")
        return [
            (key, self[key])
            for key in order
            if key in self and (key not in default or self[key] != default[key])
        ]


class YamlWargear(dict):
    def to_omap(self):
        order = ["special", "weapons", "text", "cost"]
        return [(key, self[key]) for key in order if key in self]


def represent_omap(dumper, data):
    return dumper.represent_mapping("tag:yaml.org,2002:map", data.to_omap())


def represent_omap_flow(dumper, data):
    return dumper.represent_mapping(
        "tag:yaml.org,2002:map", data.to_omap(), flow_style=False
    )


def format_default(data):
    return yaml.dump(data)


def format_equipments(data):
    yaml.add_representer(YamlEquipments, represent_omap)
    yaml.add_representer(YamlWeapon, represent_omap_flow)
    yaml.add_representer(YamlWargear, represent_omap_flow)

    data["weapons"] = {k: YamlWeapon(v) for k, v in data["weapons"].items()}
    data["wargear"] = {k: YamlWargear(v) for k, v in data.get("wargear", {}).items()}
    newdata = YamlEquipments(data)
    return yaml.dump(newdata, default_flow_style=None)


def format_models(data):
    yaml.add_representer(YamlModel, represent_omap)
    newdata = {name: YamlModel(model) for name, model in data.items()}
    return yaml.dump(newdata, default_flow_style=None)


def format_unit_composition(data):
    if data == "self":
        return data
    return [YamlUnit(comp) for comp in data]


def format_sections(data):
    return YamlSection(
        {name: format_unit_composition(comps) for name, comps in data.items()}
    )


def format_units(data):
    yaml.add_representer(YamlUnit, represent_omap)
    yaml.add_representer(YamlSection, represent_omap_flow)
    newdata = {section: format_sections(units) for section, units in data.items()}
    return yaml.dump(newdata, default_flow_style=None)


def format_upgrades(data):
    yaml.add_representer(YamlUpgrade, represent_omap)
    newdata = [YamlUpgrade(group) for group in data]
    return yaml.dump(newdata, default_flow_style=None)


def help_print_diff(filename, old, new):
    for number, (old_line, new_line) in enumerate(
        zip(old.splitlines(), new.splitlines())
    ):
        if old_line == new_line:
            continue
        print("Error in file {}".format(filename))
        print("Line {}".format(number))
        print("- {}".format(old_line))
        print("+ {}".format(new_line))


def format_file(filename, path, dry, format_func):
    floc = os.path.join(path, filename)
    print("processing {0}".format(floc))
    with open(floc, "r") as f:
        rawdata = f.read()
        data = yaml.safe_load(rawdata)

    newdata = format_func(data)

    if newdata != rawdata:
        if dry:
            help_print_diff(filename, rawdata, newdata)
            sys.exit(1)
        os.rename(floc, floc + "~")
        with open(floc, "w") as f:
            f.write(newdata)


def indent(faction, dry):
    if not os.path.isdir(faction):
        return
    yamlFiles = [f for f in os.listdir(faction) if f.endswith(".yml")]
    for f in yamlFiles:
        if f == "equipments.yml":
            format_file(f, faction, dry, format_equipments)
        elif f == "rules.yml":
            format_file(f, faction, dry, format_default)
        elif f == "models.yml":
            format_file(f, faction, dry, format_models)
        elif f == "units.yml":
            format_file(f, faction, dry, format_units)
        elif f == "upgrades.yml":
            format_file(f, faction, dry, format_upgrades)
        elif f == "psychics.yml":
            format_file(f, faction, dry, format_default)


def default_path():
    factions = [
        Path(d.name)
        for d in os.scandir("Factions")
        if d.is_dir() and "models.yml" in [f.name for f in os.scandir(d)]
    ]
    return ["Factions" / f for f in factions] + ["Factions"]


def main():

    parser = argparse.ArgumentParser(
        description="Indent the yaml source files, to ease editing them by hand, and avoid to much useless diffs"
    )
    parser.add_argument(
        "path",
        metavar="path",
        type=str,
        nargs="*",
        help="path to the faction to indent all yaml",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="do not modify file, but return an error if one file needs modification",
    )

    args = parser.parse_args()
    paths = args.path or default_path()
    for path in paths:
        indent(path, args.dry_run)


if __name__ == "__main__":
    # execute only if run as a script
    main()

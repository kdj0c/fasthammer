# protocol :
# base64, 6bits per character
# 9 bits symbols (2 symbols in 3 ascii char)
# Faction:  0000xxxxx (32)
# Reserved: 0001xxxxx (32)
# Upgrade:  0010xxxxx (32)
# Reserved: 0011xxxxx (32)
# Count:    010xxxxxx (64)
# Reserved: 011xxxxxx (64)
# Unit:     10xxxxxxx (128)
# Model:    11xxxxxxx (128)
# Example of army list:
# Faction(xxx)Units(xx)Model(xx)Count(xx)Upgrade(xx)Count(xx)Units(xx)Model(xx)Model(xx)Units(xx)Model(xx)

convert = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"


def from_64(s):
    c = ord(s)
    if c == 45:
        return 62
    if c <= 57:
        return c + 4
    if c <= 90:
        return c - 65
    if c == 95:
        return 63
    return c - 71


def char2symbol(b64str):
    l64 = [from_64(s) for s in b64str]
    out = []

    while len(l64) > 2:
        a, b, c = l64[:3]
        ub = int(b / 8)
        lb = b % 8
        out.extend([(a * 8) + ub, (lb * 64) + c])
        l64 = l64[3:]
    if len(l64) == 2:
        a, b = l64
        out.extend([(a * 8 + int(b / 8))])
    return out


def symbol2char(l512):
    out = []
    while len(l512) > 1:
        a, b = l512[:2]
        out.extend([int(a / 8), (a % 8) * 8 + int(b / 64), b % 64])
        l512 = l512[2:]
    if len(l512) == 1:
        a = l512[0]
        out.extend([int(a / 8), (a % 8) * 8])
    return "".join([convert[x] for x in out])


def to_symbol(value, kind):
    if kind == "faction":
        return value
    elif kind == "upgrade":
        return value + 64
    elif kind == "count":
        return value + 128
    elif kind == "unit":
        return value + 256
    elif kind == "model":
        return value + 384


class Symbol:
    def __init__(self, value):
        if value < 32:
            self.value = value
            self.type = "faction"
        elif 64 <= value < 96:
            self.value = value - 64
            self.type = "upgrade"
        elif 128 <= value < 192:
            self.value = value - 128
            self.type = "count"
        elif 256 <= value < 384:
            self.value = value - 256
            self.type = "unit"
        elif 384 <= value < 512:
            self.value = value - 384
            self.type = "model"

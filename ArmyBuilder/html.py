def htmlClass(tag, hclass, content):
    return f"<{tag} class={hclass}>{content}</{tag}>"


def htmlTag(tag, content):
    return f"<{tag}>{content}</{tag}>"


def htmlAttrib(tag, attrib, content):
    return f"<{tag} {attrib}>{content}</{tag}>"


def button(id, name, callback, disabled=False):
    b_id = f"{callback}_{id}"
    onclick = f"army_builder.armyBuilder.{callback}(this.id)"
    attrib = f'id="{b_id}" onclick="{onclick}"'
    if disabled is True:
        attrib += " disabled"
    return htmlAttrib("button", attrib, name)


def svg_icon(tag):
    return htmlClass("svg", "icon", f'<use href="#{tag}"/>')


# use console.log() with transcrypt, and print with CPython
def dbg(s):
    #? console.log(s)
    print(s)  # __:skip


#!/usr/bin/env python3

"""
Copyright 2019 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


import os
from sys import exit

import yaml

from schema import equipmentsSchema, modelSchema, rulesSchema, unitSchema, upgradeSchema

errors = []

std_rules = [
    "Hero",
    "Monster",
    "Vehicle",
    "Aircraft",
    "Speed",
    "HP",
    "CC",
    "BS",
    "Defense",
]


def on_error(s):
    errors.append(s)


def get_rule_name(s):
    if "(" in s:
        return s[: s.find("(")]
    if "+" in s:
        return s[: s.find("+")]
    if "-" in s:
        off = s.find("-")
        if s[off + 1 :].isdigit():
            return s[:off]
    return s


# Remove 3x and Twin from weapon name
def get_weapon_name(s):
    while " " in s:
        firstword, remaining = s.split(" ", 1)
        if firstword.endswith("x") and firstword[:-1].isdigit():
            s = remaining
            continue
        if firstword == "Twin":
            s = remaining
        break
    return s


def read_yaml(dir, filename):
    fname = os.path.join(dir, filename)
    with open(fname, "r") as f:
        return yaml.safe_load(f.read())


class ModelChecker:
    def __init__(self, weapons, wargears, rules):
        self.weapons = weapons
        self.wargears = wargears
        self.rules = rules
        self.used_weapons = []
        self.used_wargears = []
        self.used_rules = []

    def check_equ(self, equ):
        for e in equ:
            name = get_weapon_name(e)
            if name in self.weapons:
                if name not in self.used_weapons:
                    self.used_weapons.append(name)
                continue
            if name in self.wargears:
                if name not in self.used_wargears:
                    self.used_wargears.append(name)
                continue
            on_error(f"equipment {name} is not defined")

    def check_rules(self, rules):
        for rule in rules:
            name = get_rule_name(rule)
            if name not in self.rules:
                on_error(f"rule {name} is not defined")
            elif name not in self.used_rules:
                self.used_rules.append(name)

    def parse_model(self, dir):
        yml = read_yaml(dir, "models.yml")
        modelSchema(yml)
        for model in yml.values():
            self.check_equ(model.get("equipment", []))
            self.check_rules(model.get("special", []))

    def parse_unit(self, dir):
        yml = read_yaml(dir, "units.yml")
        unitSchema(yml)

    def parse_upgrades(self, dir):
        yml = read_yaml(dir, "upgrades.yml")
        upgradeSchema(yml)
        for up in yml:
            for equ in up["add"]:
                self.check_equ(equ)

    def update_rules(self):
        for name in self.used_weapons:
            for weapon in self.weapons[name]:
                special = weapon.get("special", [])
                self.check_rules(special)
        for name in self.used_wargears:
            special = self.wargears[name].get("special", [])
            self.check_rules(special)

    def check_unused(self):
        self.update_rules()
        unused_weapons = [w for w in self.weapons if w not in self.used_weapons]
        unused_wargears = [w for w in self.wargears if w not in self.used_wargears]
        unused_rules = [w for w in self.rules if w not in self.used_rules]
        for w in unused_weapons:
            on_error(f"Unused weapon {w}")
        for w in unused_wargears:
            on_error(f"unused wargears {w}")
        for r in unused_rules:
            if r not in std_rules:
                on_error(f"unused rules {r}")


class RulesInventory(dict):
    def add_standard_rules(self):
        for r in std_rules:
            self[r] = "defined"

    def parse(self, dir):
        yml = read_yaml(dir, "rules.yml")
        rulesSchema(yml)
        rules = yml["specialRules"]
        for name, desc in rules.items():
            n = get_rule_name(name)
            if n in self:
                on_error(f"rules already defined {n} in {dir}")
                continue
            self[n] = desc


class WeaponInventory(dict):
    @staticmethod
    def check(name, w):
        if "special" in w and type(w["special"]) is not list:
            raise TypeError(f"weapon {name} should be a list")

    def parse(self, dir):
        yml = read_yaml(dir, "equipments.yml")
        equipmentsSchema(yml)

        weapons = yml["weapons"]
        for name, w in weapons.items():
            self.check(name, w)
            n, _, _ = name.partition("@")
            if n in self:
                on_error(f"Weapon {n} Already Defined")
        for name, w in weapons.items():
            n, _, _ = name.partition("@")
            if n in self:
                self[n].append(w)
            else:
                self[n] = [w]


class WargearInventory(dict):
    def parse(self, dir):
        yml = read_yaml(dir, "equipments.yml")
        wargears = yml.get("wargear", {})
        for name, w in wargears.items():
            if name in self:
                on_error(f"Wargear {name} Already Defined")
        for name, w in wargears.items():
            self[name] = w


def get_factions():
    for dir in os.scandir("Factions"):
        if not dir.is_dir():
            continue
        files = os.scandir(dir)
        if "models.yml" in [f.name for f in files]:
            yield dir.path


def main():
    fdirs = [f for f in get_factions()]
    print(fdirs)
    weapons = WeaponInventory()
    wargears = WargearInventory()
    rules = RulesInventory()
    rules.add_standard_rules()

    all = [weapons, wargears, rules]

    for inv in all:
        inv.parse("Factions")
    for d in fdirs:
        for inv in all:
            inv.parse(d)
    uc = ModelChecker(weapons, wargears, rules)
    for d in fdirs:
        uc.parse_model(d)
        uc.parse_unit(d)
        uc.parse_upgrades(d)
    uc.check_unused()

    if errors:
        print(f"Checks done, found {len(errors)} errors:")
        for e in errors:
            print(e)
        exit(1)
    print("Checks done, no error found")


if __name__ == "__main__":
    # execute only if run as a script
    main()

from validx import Any, Bool, Dict, Int, List, OneOf, Str

Dice = Str(pattern="^\d+(d\d+([+]\d+)?)?$")

# rules.yml
rulesSchema = Dict(
    {
        "rulesCost": Dict(extra=(Str(), Int())),
        "specialRules": Dict(extra=(Str(), Str())),
    },
    defaults={"rulesCost": {}, "specialRules": {}},
)

# equipments.yml
weapons = Dict(
    {
        "range": Int(),
        "attacks": OneOf(Dice, Int()),
        "ap": OneOf(Dice, Int()),
        "damage": OneOf(Dice, Int()),
        "special": List(Str()),
    },
    optional=["range", "ap", "damage", "special"],
)
wargears = Dict(
    {"special": List(Str()), "text": Str(), "cost": Int()},
    optional=["special", "text", "cost"],
)
equipmentsSchema = Dict(
    {
        "weapons": Dict(extra=(Str(), weapons)),
        "wargear": Dict(extra=(Str(), wargears)),
    },
    defaults={"weapons": {}, "wargears": {}},
)

# models.yml
model = Dict(
    {
        "speed": Int(),
        "cc": Int(),
        "bs": Int(),
        "defense": Int(),
        "hitpoints": Int(),
        "equipment": List(Str()),
        "special": List(Str()),
    },
    optional=["equipment", "special", "hitpoints"],
)
modelSchema = Dict(extra=(Str(), model))

# units.yml
models = Dict(
    {"models": List(Str()), "min": Int(), "max": Int()}, optional=["min", "max"]
)
unitcomposition = List(models)
units = Dict(extra=(Str(), OneOf(Str(options=["self"]), unitcomposition)))
unitSchema = Dict(extra=(Str(), units))

# Upgrades.yml
upgrades = Dict(
    {
        "models": List(Str()),
        "add": List(List(Str())),
        "remove": List(Str()),
        "all": Bool(),
        "max": Int(),
        "max_model": OneOf(Int(), Str()),
        "text": Str(),
    },
    optional=["remove", "all", "max", "max_model", "text"],
)
upgradeSchema = List(upgrades)

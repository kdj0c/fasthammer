### General Principles

* **The most important rule:** Whenever the rules are
unclear or don't seem quite right, use common
sense and personal preference. Have fun!
* **Skill Tests:** Whenever you must take a Skill
test, roll one six-sided die trying to score
the skill value or higher, which counts as a success.
An unmodified result of 1 is always a failure. An unmodified result of 6 is always a success, even if you need a 7+ or higher.
* **Defense Tests:** It works like a Skill Test, trying to score the Defense value or higher.
However, if you need to roll a 7+ or higher to succeed against a Defense, then you must roll a 6+ followed by a 4+.
* **Line of Sight:** If you can draw a straight line from
an attacker to any part of his target, then it has line of sight.
* **Overruling:** An army rule always overrules the common rule.

### Preparation

* **The Battlefield:** The game is played on a flat 6'x4'
surface, with at least 5-10 pieces of terrain on it.
* **The Armies:** Before the game, players must agree on the
value in points for the armies and use the ArmyBuilder to choose
which models and units will fight the battle. Each army must
be led by a Hero designated as the Warlord.
* **Mission:** Choose (or roll) a mission in the mission list and follow the instructions.
* **Victory:** After 4 rounds, the game ends. The player that
has the highest number of Victory Points wins. If both players
have an equal number of Victory Points, it's a tie.

### Playing the Game

The game is played in rounds in which players
alternate in activating one unit each until all units
have been activated. The player who finished deploying
first starts activating first on the first round. Each
new round the player that finished activating first
in the previous round gets to activate first.

### Activation

The player declares which unit he activates, and it
may do one of the following:

| Action | Move | Notes |
|---|---|---|
| Hold | 0" | May Shoot then Charge. |
| Advance | speed | May Shoot after moving then Charge. |
| Rush | speed&nbsp;+&nbsp;4" | May not shoot nor Charge. |

### Movement

All members of a unit must stay in cohesion by remaining within 2" of at
least one other member and within 6" of all other
members at all times. Units may only move within
1" of enemy units when charging. A unit can move through friendly Infantry units and any Aircraft.

### Shooting

All models that are in range and have a line of sight
to an enemy unit may fire all of their weapons at it. You must declare the target for every weapon of every model in the unit before shooting.
You may fire the weapons in whatever order you want.  
Shooting models take one Ballistic Skill (BS) Test per attack
and each success is a hit on the target unit.

### Hits

For each hit, the controlling player must take one Defense Test, and each success causes a wound.
If the weapon has Armor Piercing (AP), the defense value is lowered by the AP.
So a model with a Defense of 6+, hit by a weapon with AP 2, is wounded on a roll of 4+.
So the defender has to roll a low score, to have his model survive.
A wound must be allocated to the model with the least remaining Hit Points.
If multiple models are eligible to receive a wound,
the unit's owner chooses which model is wounded.
The wounded model loses one Hit point per Damage point inflicted by the weapon.
When the model's Hit points reaches 0 or less, the model is removed (excess damage is lost).
Repeat the process until all wounds are allocated or there is no remaining model.

### Charging

You can charge an enemy unit up to 7" far. A unit may only charge if at least one model can be moved in base contact with the target.
Once in contact, the unit may consolidate by moving by up to 3". All
attackers in contact with an enemy or 1" away from a friendly attacker in contact may strike with
all melee weapons, which works like shooting, but
using Close Combat (CC) instead of BS.
Then any remaining defenders may strike back in
the same way. Once both sides have attacked,
the attacker must move back by 1".
If one of the two units is destroyed, the other
may move by up to 3". You can charge multiple enemy units as long as the unit
cohesion is respected, and you declare the target for every weapon of every
model before charging.

### Weapons

To have shorter weapon descriptions, the default values are not written.
The default is:  a range of 0" (melee weapon), 1 attack, no Armor Piercing (AP), 1 damage and no special rule.  
Weapon profiles are listed directly on the unit's profile and are represented like this:

### WEAPONS GENERATED LATER ###

The **Knife** has 2 attacks in melee (no range specified) doesn't have Armor Piercing, will do 1 damage for each wound inflicted.  
The **Basic Rifle** has 1 attack at 24", no AP, and 1 damage.  
The **MegaAxe** has 8 attacks in melee, an AP of 5, will do 3 damages for each wound inflicted, and has the Balanced special rule.  
The **MegaWeapon** has 2d3 attacks, an AP of 3, and will do 1d3 damages and has the Assault special rule.  
The **Combo** is a special weapon which have 2 firing profile. When firing you must choose between the 'Focused' or the 'Frag' profile.
If the Combo weapon has a melee and a range profile, you may use the range profile for shooting, and then charge with the melee profile.  
All models without melee weapon, have 1 attack in melee.

### Quality

A Quality Test works like a Skill Test. When a unit should take a Quality Test, use the best(lowest) of CC or BS.

### Cover

Units that shoot at enemies in cover get -1 to their Shooting rolls.
A unit is considered in cover in the following cases (cover is not cumulative) :

* Most models are within or behind Cover Terrain.
* Most models are on Higher Ground than the units shooting at them.
* Most models are partially hidden by friendly models.

Firing models that are less than 6" from their target can ignore cover.

### Unit Types

* **Heroes:** Heroes are usually deployed with loyal bodyguards. You cannot allocate a hit or
a wound to a hero while there are still models in its unit.
* **Infantry:** Any unit that is not a Vehicle, a Monster, a Titan or an Aircraft
 count as Infantry. Models with a small base (25 or 32mm) count
as 1 in transports. Models with a medium base (40mm) count as 2 in transports.
Other models cannot embark in transports.
* **Vehicles/Monsters:** Distances are measured from the hull of a Vehicle
or any part of a Monster.  
* **Fatboy:** Any model that have 10 or more Hit Points gets the Fatboy
rule: when it is down to half or less of its total hit points value,
its movement value is halved (rounding up) and it gets -1 to its
Attack rolls when using melee and ranged weapons.
* **Titan:** Any model that have 20 or more Hit Points gets the Titan and Fatboy rule: Titans never benefit from cover.
* **Aircraft:** These units automatically have the Flying rule.
Units targeting an Aircraft get -1 to their shooting rolls.
An Aircraft can only be engaged in melee by units with the Flying rule.
When activated the model can rotate up to 90&deg; and then move at least a full 20"
in a straight line, and if this puts it on top
of other units/terrain it must keep moving until it
has space. If this move brings it outside the table
then its activation ends and it may be placed back
on any table edge.

### Special Rules

### SPECIAL RULES GENERATED LATER ###

### Dispelling

When an enemy Psychic within 24" of one of your
Psychic tries to cast a spell, you may try to block it.
Roll a die, if the result is higher than the enemy
Psychic's result, then the spell is blocked
and its effects are not resolved. A Psychic can
block only one spell each round.

### Missions

Players can agree on a mission to play or roll a die to select a random mission in the following table:

Result | Mission
--- | ---
1 | Retrieval
2 | Desperate Hold
3 | The Scouring
4 | Into the Breach
5 | Secure and Control
6 | Siege

* **Placing Objectives:** The players who won the roll-off places the first objective marker. Then the players alternate in placing one marker each over 12" away from other markers and over 6" away from the edge of the battlefield.
* **Seizing Objectives:** The player with the highest number of models within 3" of a objective marker controls it. Aircraft cannot control objectives.
* **1 - Retrieval:** Roll-off, place 4 objectives and deploy. Use the Standard Victory Points.
* **2 - Desperate Hold:** Roll deployment between **Dawn of War** and **Hammer and Anvil**, then place 3 objectives evenly at the same distance from both deployment zone. Roll-off and start deployment. At the end of each round, each player score 1 point for each objective they control. Also use **Slay the Warlord** and **First Blood** from the Standard Victory Points.
* **3 - The Scouring:** Roll-off, place 6 objectives and deploy. After deployment, randomly choose one Superior Objective worth 4 Victory Points and one Inferior Objective worth 2 Victory points. Use the Standard Victory Points.
* **4 - Into the Breach:** Roll-off and the winner chooses who will be the Attacker and who will be the Defender. Use the Hammer and Anvil deployment. The Defender picks a deployment zone and deploys all his units. Then the Attacker deploys all his units in the opposite deployment zone. The Defender starts activating first. Use the Standard Victory Points ignoring Line Breaker. The Defender gets 1 Victory Point for each enemy unit destroyed. The Attacker gets 1 Victory point for each friendly unit within 24" of the Defender's short table edge at the end of the game and 1 additional Victory point if within 12".
* **5 - King of the Hill:** Place 1 objective at the center. Roll-off, place 4 additional objectives and deploy. The center objective is worth 6 Victory points but it can only be seized by the Warlord. Use Standard Victory Points.
* **6 - Siege:** Roll-off and the winner chooses who will be the Attacker and who will be the Defender. Use the Dawn of War deployment. The Defender picks a deployment zone, places 2 objectives within it and deploys all his units. Then the Attacker deploys all his units in the opposite deployment zone. The Defender starts activating first. Use the Standard Victory Points but only the Attacker can score the Line Breaker.

### Deployment

The player who won the roll-off must roll a die to select a random deployment map in the following table:

Result | Deployment map
--- | ---
1 | Dawn of War
2 | Search and Destroy
3 | Hammer and Anvil

* **1 - Dawn of War:** The player picks one long table edge and can deploy within 12" of his table edge. His opponent takes the opposite long table edge.
* **2 - Search and Destroy:** Divide the table into four quarters. The player picks one quarter and can deploy within this quarter and 9" away from the table center. His opponent takes the opposite quarter.
* **3 - Hammer and Anvil:** The player picks one short table edge and can deploy within 24" of his table edge. His opponent takes the opposite short table edge.

Then the players alternate in placing one unit each within their deployment zone until all units are placed on the table, starting with the player who pick its deployment zone. Remaining units are kept in reserve and must have Ambush, Scout or similar ability to be deployed later. Units embarked are deployed with the transport.

The player who won the roll-off start activating first.

### Standard Victory Points

After 4 rounds, the game ends. The player who has the highest number of Victory Points wins. If both players have an equal number of Victory Points, it's a tie. Unless specified otherwise by the mission, Victory Points are achieved for the following:

* **Take and Hold:** Players get 3 Victory Points for each objective marker they control.
* **Slay the Warlord:** Players get 1 Victory Point if the enemy Warlord is dead.
* **First Blood:** Players get 1 Victory Point if they destroyed a unit during the first Round.
* **Line Breaker:** Players get 1 Victory Point if they have at least one model within the enemy deployment zone at the end of the game.

### FAQ

#### Special Rules applies to model or unit ?

Most Special rules applies to model, but some rules like Stealth, Ambush, Scout apply to unit only if all models have this special rule.
Also Some Hero's bonus applies to the whole units.
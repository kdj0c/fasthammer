#!/usr/bin/env python3

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from ArmyBuilder.armory import Weapon


def getWeapon(name="dummy", range=0, attacks=1, ap=0, damage=1, special=None):
    if not special:
        special = []
    return Weapon(name, [range, attacks, ap, damage, special])


class DummyModel:
    def __init__(self, bs=3, cc=3, weapon_rules=None, speed=6):
        self.bs = bs
        self.cc = cc
        self.weapon_rules = weapon_rules or []
        self.speed = speed


dmodel = DummyModel()


# N attacks should cost N times the cost of the single attack weapon
def test_Weapon_attacks():
    knife = getWeapon(attacks=1)
    dknife = getWeapon(attacks=2)
    assert 2 * knife.cost(dmodel) == dknife.cost(dmodel)


def test_Psychics():
    punch = getWeapon(range=18, attacks="1d3", special=["Deadly"])
    # Psychic cost is 25, should be at least equivalent to this weapon
    assert 23 < punch.cost(dmodel) < 27


# check that 5+ quality weapon is twice the cost of 6+
def test_Weapon_quality():
    rifle = getWeapon(range=24)
    ork = DummyModel(bs=5)
    gretchin = DummyModel(bs=6)
    assert rifle.cost(ork) == rifle.cost(gretchin) * 2


# Rending(4) is like AP(4) if quality is 6+
def test_Weapon_rending():
    knife = getWeapon(ap=4)
    rknife = getWeapon(special=["Rending(4)"])
    gretchin = DummyModel(cc=6)
    assert knife.cost(gretchin) == rknife.cost(gretchin)


# Cost of Balanced weapon is like having +1 in quality
def test_Weapon_balanced():
    knife = getWeapon()
    good_knife = getWeapon(special=["Balanced"])
    clumsy = DummyModel(cc=4)
    assert knife.cost(dmodel) == good_knife.cost(clumsy)


def test_Weapon_Rapid_Fire():
    rifle = getWeapon(range=12)
    pistol = getWeapon(range=6)
    rapidfire = getWeapon(range=12, special=["Rapid Fire"])
    assert rifle.cost(dmodel) + pistol.cost(dmodel) == rapidfire.cost(dmodel)
